package user

import (
	"context"
	"test_arch_go/internal/domain/model"
)

type UserRepo interface {
	FindUserByEmail(ctx context.Context, email string) (model.UserModel, error)
}
