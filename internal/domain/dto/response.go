package dto

type UserResponse struct {
	
}

type ErrorResponse struct {
	Status string   `json:"status"`
	Error  []string `json:"error,omitempty"`
}
