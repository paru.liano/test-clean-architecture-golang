package api

import (
	"github.com/gofiber/fiber/v2"
)

var (
	addr = ":8080"
)

func main() {

	app := fiber.New()

	r := Routes{}

	r.RegisterRoutes()
	err := app.Listen(addr)
	if err != nil {
		panic(err)
	}

}
