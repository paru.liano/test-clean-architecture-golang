package api

import (
	"test_arch_go/internal/handler"

	"github.com/gofiber/fiber/v2"
)

type Routes struct {
	App     *fiber.App
	handler *handler.Handler
}

func (r *Routes) RegisterRoutes() {
	routes := r.App.Group("/api/v1")

	routes.Get("/example", func(c *fiber.Ctx) error {
		return c.SendString("I'm a POST request!")
	})

}
